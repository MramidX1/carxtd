﻿using UnityEngine;

public class ApplicationManager : MonoBehaviour
{
    [Tooltip("Генератор мобов")]
    [SerializeField]private Spawner m_enemiesSpawner;

    public static ApplicationManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public Spawner GetSpawner()
    {
        return m_enemiesSpawner;
    }
}
