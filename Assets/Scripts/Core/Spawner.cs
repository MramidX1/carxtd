﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(PoolObjects))]

public class Spawner : MonoBehaviour
{
	[Tooltip("Интервал между выстрелами")]
	[SerializeField] private float m_interval = 3;

	[Tooltip("Конечная точка для мобов")]
	[SerializeField] private GameObject m_moveTarget;

	private PoolObjects m_poolObjects;
	private float m_lastSpawn = -1;

	private void Awake() 
	{
		m_poolObjects = GetComponent<PoolObjects>();
		m_poolObjects.Init();	
	}

	void Update() 
	{
		SpawnProcess();
	}

	private void SpawnProcess()
	{
		var intervalChecker = Time.time > m_lastSpawn + m_interval;
		if (intervalChecker) 
		{
			var newMonster = m_poolObjects.GetObject();
			var monsterBeh = newMonster.GetComponent<Monster>();

			newMonster.transform.position = transform.position;
			
			monsterBeh.SetMoveTarget(m_moveTarget);
			monsterBeh.SetPoolObjects(m_poolObjects);

			m_lastSpawn = Time.time;
		}
	}

	public List<GameObject> GetPoolObjects()
	{
		return m_poolObjects.GetPoolObjects();
	}
}
