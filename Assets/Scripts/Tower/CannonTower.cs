﻿using UnityEngine;

public class CannonTower : BaseTower 
{
	[SerializeField] private float m_rotateSpeed = 10f;

	[SerializeField] private GameObject m_cannon;
	[SerializeField] private Transform m_shootPoint;
	private CannonProjectile m_projectile;

	private bool m_targetInFocus;

	private Vector3 interceptionPoint = new Vector3();
	private Vector3 previosTargetPos = new Vector3();

	private const float SHOOTPAUSE = 0.4f;
	private float m_shootPauseTemp = 0f;

    protected override void LockTarget(GameObject target)
    {		
		m_target = target;

		if (m_projectile == null)
		{
			m_projectile = m_poolObjects.GetObject().GetComponent<CannonProjectile>();
			m_projectile.gameObject.SetActive(false);
		}

		CalculateInterceptionPoint(m_shootPoint.position, m_projectile.GetSpeed(), m_target.transform.position);
		RotateToTarget();
		CheckShootReady();
    }

	private void CheckShootReady()
	{
		if(!m_target)
		{
			return;
		}

		var directoinToCannon = (m_cannon.transform.position - interceptionPoint).normalized;
		var cannonForward = m_cannon.transform.forward;

		var dotResult = Vector3.Dot(directoinToCannon, cannonForward);

		if(dotResult < -0.9f)
		{
			m_targetInFocus = true;
		}
		else
		{
			m_targetInFocus = false;
		}
	}

    protected override void Shoot()
    {
		if(!m_targetInFocus)
		{
			return;
		}

		if(m_projectile)
		{
			m_projectile.transform.position = m_shootPoint.position;
			m_projectile.SetProjectileTarget(m_target);
			m_projectile.SetPoolObjects(m_poolObjects);
			m_projectile.SetTargetPoint(interceptionPoint);
			m_projectile.gameObject.SetActive(true);
			m_projectile.StartMove();
		}

		m_projectile = null;

		m_shootPauseTemp = SHOOTPAUSE;
    }


	public void CalculateInterceptionPoint(Vector3 shootPosition, float speedProjectile, Vector3 targetPosition)
    {
		var targetVelocity = (m_target.transform.position - previosTargetPos);

		Vector3 directionToShootPoint = shootPosition - targetPosition;

		var speedWithScale = targetVelocity.magnitude;

		var a = Mathf.Pow(speedProjectile, 2) - Mathf.Pow(speedWithScale, 2);
		var b = 2 * Vector3.Dot(directionToShootPoint, targetVelocity);
		var c = -Vector3.Dot(directionToShootPoint, directionToShootPoint);

		if ((Mathf.Pow(b, 2) - (4 * (a * c))) < 0)
		{
			interceptionPoint = Vector2.zero;
		}

		var t = (-(b) + Mathf.Sqrt(Mathf.Pow(b, 2) - (4 * (a * c)))) / (2 * a);

		interceptionPoint = ((t * (targetVelocity)) + targetPosition);
		previosTargetPos = m_target.transform.position;
    }

	private void RotateToTarget()
	{
		if(!m_target
		|| !m_cannon)
		{
			return;
		}

		if (m_shootPauseTemp > 0)
		{
			m_shootPauseTemp -= Time.deltaTime;
			return;
		}

		Vector3 targetDirection = (interceptionPoint - m_cannon.transform.position).normalized;
		var targetRotation = Quaternion.LookRotation(targetDirection);
		m_cannon.transform.rotation = Quaternion.RotateTowards(m_cannon.transform.rotation, targetRotation , Time.deltaTime * m_rotateSpeed);

		var localEulerAngles = m_cannon.transform.localEulerAngles;
		m_cannon.transform.localEulerAngles = new Vector3(0, localEulerAngles.y, 0);

	}
}
