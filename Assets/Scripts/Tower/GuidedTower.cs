﻿using UnityEngine;
using System.Collections;

public class GuidedTower : BaseTower 
{
	protected override void LockTarget(GameObject target)
	{
		m_target = target;
	}

    protected override void Shoot()
    {
        var projectile = m_poolObjects.GetObject();
		projectile.transform.position = transform.position + Vector3.up * 1.5f;
		projectile.transform.rotation = Quaternion.identity;

		var projectileBeh = projectile.GetComponent<BaseProjectile> ();
		if(projectileBeh)
		{
			projectileBeh.SetProjectileTarget(m_target);
			projectileBeh.SetPoolObjects(m_poolObjects);
			projectileBeh.StartMove();
		}
    }
}
