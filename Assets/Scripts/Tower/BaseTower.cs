﻿using UnityEngine;

[RequireComponent(typeof(PoolObjects))]
public class BaseTower : MonoBehaviour
{
    [Tooltip("Интервал выстрела")]
    [SerializeField] protected float m_shootInterval = 0.5f;
    
    [Tooltip("Дальнобойность башни")]
	[SerializeField] protected float m_range = 4f;

    protected PoolObjects m_poolObjects;
    protected GameObject m_target;
    protected float m_lastShotTime = -0.5f;


    private void Start()
    {
        m_poolObjects = GetComponent<PoolObjects>();
        m_poolObjects.Init();
    }

    private void FixedUpdate()
    {
        OnFixedUpdate();
        ShootingCheck();
    }

    private void ShootingCheck()
    {
	    var spawner = ApplicationManager.Instance.GetSpawner();
	    var allEnemies = spawner.GetPoolObjects();

        foreach (var enemie in allEnemies) 
		{
			if (!enemie.activeInHierarchy)
			{
				continue;
			}

			var distanceChecker = Vector3.Distance (transform.position, enemie.transform.position) > m_range;
			var timeChecker = m_lastShotTime + m_shootInterval > Time.time;

			if (distanceChecker)
			{
				continue;
			}

            LockTarget(enemie.gameObject);

			if (timeChecker)
			{
				continue;
			}

            Shoot();

            m_lastShotTime = Time.time;
        }
    }

    protected virtual void LockTarget(GameObject target){}
    protected virtual void Shoot(){}
    protected virtual void OnFixedUpdate(){}
}
