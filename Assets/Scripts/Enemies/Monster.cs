﻿using UnityEngine;

public class Monster : MonoBehaviour 
{
	[Tooltip("Скорость полета")]
	[SerializeField] private float m_speed = 0.1f;

	[Tooltip("Максимальное количество жизней")]
	[SerializeField] private int m_maxHP = 25;

	[Tooltip("Текущее количество жизней")]
	[SerializeField] private int m_currentHp;

	const float REACHDISTANCE = 0.3f;

	
	private GameObject m_moveTarget;
	private PoolObjects m_poolObjects;

	private void Start() 
	{
		m_currentHp = m_maxHP;
	}

	private void FixedUpdate () 
	{
		if (m_moveTarget == null)
		{
			return;
		}
		
		var distanceChecker = Vector3.Distance (transform.position, m_moveTarget.transform.position) <= REACHDISTANCE;
		if (distanceChecker) 
		{
			ResetAnReturnToPool();
			return;
		}

		var translation = m_moveTarget.transform.position - transform.position;
		if (translation.magnitude > m_speed) 
		{
			translation = translation.normalized * m_speed;
		}

		transform.Translate (translation);
	}

	public void SetPoolObjects(PoolObjects pool)
	{
		m_poolObjects = pool;
	}

	public void ResetAnReturnToPool()
	{
		m_currentHp = m_maxHP;
		m_poolObjects.ReturnObject(gameObject);
	}

	public void SetMoveTarget(GameObject newTarget)
	{
		m_moveTarget = newTarget;
	}

	public void SetDamage(int damage)
	{
		m_currentHp -= damage;
		
		var hpChecker = m_currentHp <= 0;
		if (hpChecker) 
		{
			ResetAnReturnToPool();
		}
	}
}
