﻿using UnityEngine;

public class CannonProjectile : BaseProjectile
{
	private Vector3 m_targetPoint;
	protected override void Move()
	{
		var dirToTarget = m_targetPoint - transform.position;
		var translation = dirToTarget.normalized * m_speed;
		transform.Translate (translation);
	}

	public void SetTargetPoint(Vector3 targetPoint)
	{
		m_targetPoint = targetPoint;
	}

	public float GetSpeed()
	{
		return m_speed;
	}
}
