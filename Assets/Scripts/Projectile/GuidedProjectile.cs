﻿using UnityEngine;
using System.Collections;

public class GuidedProjectile : BaseProjectile 
{
	protected override void Move()
	{
		var translation = m_target.transform.position - transform.position;
		if (translation.magnitude > m_speed) 
		{
			translation = translation.normalized * m_speed;
		}

		transform.Translate (translation);
	}
}
