﻿using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
	[Tooltip("Скорость полета снаряда")]
	[SerializeField] protected float m_speed = 0.2f;

	[Tooltip("Наносимый снарядом урон")]
	[SerializeField] public int m_damage = 10;

	protected GameObject m_target;
	protected PoolObjects m_poolObjects;

	private bool m_startMove;

	private void FixedUpdate () 
	{
		if (m_target == null
		|| !m_target.activeInHierarchy) 
		{
			m_poolObjects.ReturnObject(gameObject);
			return;
		}

		if(m_startMove)
		{
			Move();
		}
	}
	
    public void OnTriggerEnter(Collider other) 
	{
		var monster = other.gameObject.GetComponentInParent<Monster>();
		if (monster == null)
		{
			return;
		}

		monster.SetDamage(m_damage);
		m_poolObjects.ReturnObject(gameObject);
	}

	public void StartMove()
	{
		m_startMove = true;
	}

	public void SetProjectileTarget(GameObject newTarget)
	{
		m_target = newTarget;
	}

	public void SetPoolObjects(PoolObjects newPool)
	{
		m_poolObjects = newPool;
	}

	protected virtual void Move(){}

}
