﻿using System.Collections.Generic;
using UnityEngine;

public class PoolObjects : MonoBehaviour
{
    [Tooltip("Шаблон объекта")]
    [SerializeField] private GameObject prefabObject;
    
    [Tooltip("Стартовый размер пула")]
    [SerializeField] private int startPoolSize = 10;

    [Tooltip("Имя для корневого объекта в иерархии")]
    [SerializeField] private string poolName = "";

    private GameObject m_parentObject;
    private List<GameObject> m_objects = new List<GameObject>();

    public void Init()
    {
        m_parentObject = new GameObject(poolName);
        
        for(int a = 0; a < startPoolSize; a++)
        {
            var newObject = Instantiate(prefabObject);
            newObject.transform.parent = m_parentObject.transform;
            newObject.SetActive(false);
            newObject.transform.position = Vector3.zero;
            
            m_objects.Add(newObject);
        }
    }

    public GameObject GetObject()
    {
        GameObject objectToReturn = GetFirstDeactive();

        if(objectToReturn == null)
        {
            var newObject = Instantiate(prefabObject);
            objectToReturn = newObject;
            m_objects.Add(objectToReturn);
        }

        objectToReturn.SetActive(true);
        return objectToReturn;
    }

    private GameObject GetFirstDeactive()
    {
        GameObject objectToReturn = null;

        foreach(var obj in m_objects)
        {
            if(!obj.activeInHierarchy)
            {
                objectToReturn = obj;
            }
        }

        return objectToReturn;
    }

    public void ReturnObject(GameObject obj)
    {
        obj.SetActive(false);
        obj.transform.position = Vector3.zero;
    }

    public List<GameObject> GetPoolObjects()
    {
        return m_objects;
    }

}
